<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Training</title>
</head>
<body>


	<h2>Add Training</h2>

	<form method="post" action="AddTraining">
		<table>
			<tr>
				<td>Training Name:</td>
				<td><input type="text" name="tName" required /></td>
			</tr>
			<tr>
				<td>Training Mode:</td>
				<td><input type="text" name="tMode" required /></td>
			</tr>
			<tr>
				<td>Start Date:</td>
				<td><input type="date" name="startDate" required /></td>
			</tr>
			<tr>
				<td>End Date:</td>
				<td><input type="date" name="endDate" required /></td>
			</tr>
			<tr>
				<td>Contact Person:</td>
				<td><input type="number" name="contactPersonID" required /></td>
			<tr>
			<tr>
				<td>Business Unit:</td>
				<td><input type="text" name="businessUnit" required /></td>
			</tr>
			<br>
			<tr>
				<td><input type="submit" value="Add Traoining" /></td>
			</tr>
		</table>
	</form>



	<hr>
<h3>List of training</h3>

<table border=1 width=1000>
<tr>
	<th>Training ID</th>
	<th>Training Name</th>
	<th>Start date</th>
	<th>End Date</th>
	<th>Training Mode</th>
	<th>Business Unit</th>
	<th>Contact Person ID</th>
</tr>

 <c:forEach items="${tList}" var = "t">

<tr>
         <td><c:out value = "${t.getTrainingID()}"/></td>
         <td><c:out value = "${t.gettName()}"/></td>
         <td><c:out value = "${t.getStartDate()}"/></td>
         <td><c:out value = "${t.getEndDate()}"/></td>
         <td><c:out value = "${t.gettMode()}"/></td>
         <td><c:out value = "${t.getBusinessUnit()}"/></td>
         <td><c:out value = "${t.getContactPersonID()}"/></td>
        
             
</tr>
      </c:forEach>

</table>
 
</body>
</html>