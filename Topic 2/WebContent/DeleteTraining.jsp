<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Delete Training</title>


<script>
	function goBack() {
		window.history.back();
	}
</script>
</head>
<body>

	<h2>Delete Training</h2>
	<c:choose>
		<c:when test="${tList.isEmpty()}">
			<p>List is empty
			<p>

				<button onclick="goBack()">Go Back</button>
			<form action="AddTraining">
				<input type="submit" value="Add Training" />
			</form>

		</c:when>
		<c:otherwise>
			<form method="post" action="DeleteTrainig">
				<table>
					<tr>
						<td>Select Training ID:</td>
						<td><select name="id">
								<c:forEach items="${tList}" var="t">
									<option value="${t.getTrainingID()}">${t.getTrainingID()}</option>

								</c:forEach>
						</select></td>
					</tr>
					<tr>

						<td><input type="submit" value="Delete Traoining"
							onclick="confirm('Confirm deletion!');" /></td>

					</tr>
				</table>
			</form>
		</c:otherwise>
	</c:choose>
	<hr>
	<h3>List of training</h3>



	<c:choose>
		<c:when test="${tList.isEmpty()}">
			<p>List is empty
			<p>
		</c:when>


		<c:otherwise>
			<table border=1 width=1000>
				<tr>
					<th>Training ID</th>
					<th>Training Name</th>
					<th>Start date</th>
					<th>End Date</th>
					<th>Training Mode</th>
					<th>Business Unit</th>
					<th>Contact Person ID</th>
				</tr>
				<c:forEach items="${tList}" var="t">
					<tr>
						<td><c:out value="${t.getTrainingID()}" /></td>
						<td><c:out value="${t.gettName()}" /></td>
						<td><c:out value="${t.getStartDate()}" /></td>
						<td><c:out value="${t.getEndDate()}" /></td>
						<td><c:out value="${t.gettMode()}" /></td>
						<td><c:out value="${t.getBusinessUnit()}" /></td>
						<td><c:out value="${t.getContactPersonID()}" /></td>
					</tr>
				</c:forEach>

			</table>
		</c:otherwise>
	</c:choose>
</body>
</html>