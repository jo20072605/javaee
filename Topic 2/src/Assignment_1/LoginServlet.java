package Assignment_1;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
        
	

	
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    
   }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String page = "";
		String error = "";
		
	

		if((username.equals("wipro")) && (password.equals("0000")))
		{
			page = "Home.jsp";			
			String message = "Welcome back, Mr "+username;
			request.setAttribute("message", message);
		
		}
		else
		{
			page = "index.jsp";
			error = "Invalid details";
			request.setAttribute("error", error);
		
			
		}
	
		RequestDispatcher rd=request.getRequestDispatcher(page);
		rd.forward(request, response);
	
	}

}
