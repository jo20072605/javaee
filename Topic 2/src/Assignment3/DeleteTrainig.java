package Assignment3;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;
 
@WebServlet("/DeleteTrainig")
public class DeleteTrainig extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public DeleteTrainig() {
		super();
		Training t = new Training();
	}


	@SuppressWarnings("unlikely-arg-type")
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if ("POST".equalsIgnoreCase(request.getMethod())) {

			int id = Integer.parseInt(request.getParameter("id"));
			

			
			Training tra = null;
			
			for(Training t : Training.gettList()) {
			
				if(t.getTrainingID() == id)
				{
					tra = t;
					break;
				}
			}
			
			int index = Training.gettList().indexOf(tra); 
			Training.gettList().remove(index);
			RequestDispatcher rd = request.getRequestDispatcher("Admin.jsp");
			rd.forward(request, response);
		}


 	
			request.setAttribute("tList", Training.gettList());
		
		
		RequestDispatcher rd = request.getRequestDispatcher("DeleteTraining.jsp");
		rd.forward(request, response);

	}

	
	
	
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);

	}

}
