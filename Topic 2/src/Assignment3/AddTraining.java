package Assignment3;

import java.io.IOException;
import java.util.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/AddTraining")
public class AddTraining extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AddTraining() {
		super();
		// TODO Auto-generated constructor stub
		Training t = new Training();
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if ("POST".equalsIgnoreCase(request.getMethod())) {
			Training t = new Training();

			final Comparator<Training> comp = (t1, t2) -> Integer.compare(t1.getTrainingID(), t2.getTrainingID());
			Training tID = Training.gettList().stream().max(comp).get();

			String tName = request.getParameter("tName");
			String tMode = request.getParameter("tMode");
			String startDate = request.getParameter("startDate");
			String endDate = request.getParameter("endDate");
			int contactPersonID = Integer.parseInt(request.getParameter("contactPersonID"));
			String businessUnit = request.getParameter("businessUnit");

			t.setTrainingID(tID.getTrainingID() + 1);
			t.settName(tName);
			t.settMode(tMode);
			t.setEndDate(startDate);
			t.setEndDate(endDate);
			t.setContactPersonID(contactPersonID);
			t.setBusinessUnit(businessUnit);

			Training.gettList().add(t);

			t = null;

			RequestDispatcher rd = request.getRequestDispatcher("Admin.jsp");
			rd.forward(request, response);

		}

		request.setAttribute("tList", Training.gettList());
		RequestDispatcher rd = request.getRequestDispatcher("AddTraining.jsp");
		rd.forward(request, response);

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

}
