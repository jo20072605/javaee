package Assignment3;

import java.util.ArrayList;
import java.util.List;

public class Training {

	private int trainingID;
	private String tName;
	private String startDate;
	private String endDate;
	private String tMode;
	private String businessUnit;
	private int contactPersonID;
	private static List<Training> tList;

	public Training() {
		super();

		tList = new ArrayList<Training>();

		tList.add(new Training(1, "JavaEE", "21/06/2019", "21/07/2019", "WebEx", "Dublin", 01256));
		tList.add(new Training(2, "Andoid", "01/01/2018", "01/01/2019", "WebEx", "Dublin", 01256));
		tList.add(new Training(3, "Docker", "21/06/2019", "21/07/2019", "WebEx", "Dublin", 01256));
		tList.add(new Training(4, "Jenkins", "21/06/2019", "21/07/2019", "WebEx", "Dublin", 01256));
		tList.add(new Training(5, ".Net", "21/06/2019", "21/07/2019", "WebEx", "Dublin", 01256));

		settList(tList);

	}

	public Training(int trainingID, String tName, String startDate, String endDate, String tMode, String businessUnit,
			int contactPersonID) {
		super();
		this.trainingID = trainingID;
		this.tName = tName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.tMode = tMode;
		this.businessUnit = businessUnit;
		this.contactPersonID = contactPersonID;
	}

	public static List<Training> gettList() {
		return tList;
	}

	public static void settList(List<Training> tList) {
		tList = tList;
	}

	public int getTrainingID() {
		return trainingID;
	}

	public void setTrainingID(int trainingID) {
		this.trainingID = trainingID;
	}

	public String gettName() {
		return tName;
	}

	public void settName(String tName) {
		this.tName = tName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String gettMode() {
		return tMode;
	}

	public void settMode(String tMode) {
		this.tMode = tMode;
	}

	public String getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	public int getContactPersonID() {
		return contactPersonID;
	}

	public void setContactPersonID(int contactPersonID) {
		this.contactPersonID = contactPersonID;
	}

}
