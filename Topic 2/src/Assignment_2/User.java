package Assignment_2;

public class User {

	
	private String _username;
	private String _passwprd;
	private	String _userType; 	
	
	public User() {
		
	}
	
	
	public User(String username, String password, String userType) {
		this._username = username;
		this._passwprd = password;
		this._userType = userType;
	}


	public String get_username() {
		return _username;
	}


	public void set_username(String _username) {
		this._username = _username;
	}


	public String get_passwprd() {
		return _passwprd;
	}


	public void set_passwprd(String _passwprd) {
		this._passwprd = _passwprd;
	}


	public String get_userType() {
		return _userType;
	}


	public void set_userType(String _userType) {
		this._userType = _userType;
	}
	
	
	
	
}
