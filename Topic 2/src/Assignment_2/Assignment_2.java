package Assignment_2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Assignment_2
 */
@WebServlet("/Assignment_2")
public class Assignment_2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */

    private static List<User> uList;

	public Assignment_2() {
        super();


        	uList = new ArrayList<User>();
        	
        	
        	uList.add(new User("Joaquim", "0000", "A"));
        	uList.add(new User("Joao", "0000", "E"));
        	uList.add(new User("Antonio", "0000", "A"));
        	uList.add(new User("Ram", "0000", "E"));
        	uList.add(new User("Manel", "0000", "E"));
        	uList.add(new User("Mario", "0000", "E"));
        	
        	setuList(uList);
        	
    }


    
    public static List<User> getuList() {
		return uList;
	}



	public void setuList(List<User> uList) {
		this.uList = uList;
	}



	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String page = "";
		User user = new User();
	
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
	
		if(ValidateUser(username,password))
			{
		
	        user = uList.stream().filter(x -> (x.get_username() == null ? username == null : x.get_username().equals(username))).findFirst().get();
			
	        
	        if(user.get_userType().equals("A"))
	        	page = "Admin.jsp";
	        
	        else if(user.get_userType().equals("E"))
	        	page = "Emp.jsp";
	        
	        else
	        	page = "404.jsp";
			}
		
        
    request.setAttribute("u", user);
	RequestDispatcher rd=request.getRequestDispatcher(page);
	rd.forward(request, response);
		
	}
	

	
	// validation method 
	public static boolean ValidateUser(String username, String password) {
		
		boolean flag = false;
		
		for(int i = 0; i < getuList().size(); i++) {
					
			flag = username.equals(getuList().get(i).get_username()) && password.equals(getuList().get(i).get_passwprd());
	
			if(flag) break;

		}
		
		return flag;
	}
	
	
	
	

}
