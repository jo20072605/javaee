package Servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;  
import java.util.Calendar;

@WebServlet("/Assignment2")
public class Assignment2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Assignment2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
		String name = request.getParameter("name");
		String message = "";

		Calendar c = Calendar.getInstance();
		int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

		if(timeOfDay >= 0 && timeOfDay < 12){
		   message = "Good Morning, " +name;        
		}else if(timeOfDay >= 12 && timeOfDay < 16){
			 message = "Good afternoon, " +name;
		}else if(timeOfDay >= 16 && timeOfDay < 21){
		     message = "Good Evening, " +name;
		}else if(timeOfDay >= 21 && timeOfDay < 24){
			 message = "Good night, " +name;
		}

		request.setAttribute("message", message);
		
		RequestDispatcher rd=request.getRequestDispatcher("/Response.jsp");
		rd.forward(request, response);
		
		
		
	}
	
	

 

}
