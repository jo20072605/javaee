package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class PrintTable
 */
@WebServlet("/PrintTable")
public class Assignment1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Assignment1() {
        super();
        // TODO Auto-generated constructor stub
      }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("<h1>Topic 1: Assignment 1</h1>");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Topic1 - Assignment 1 Servelet1</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<table>");
            for(int i = 0; i < 25; i++) {
            	out.print("<tr>");
            	for(int j = 0; j < 3; j++) {
            		 out.print("<td>Row"+i+" Col"+j+"</td> ");
            	}
            	  out.print("</tr>");
            }
            out.println("</table>"); 
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
