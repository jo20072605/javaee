<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Employee</title>
</head>
<script>
function goBack() {
  window.history.back();
}
</script>

<body>

<h1>Add Employee</h1>


<div id="formDiv">
	<form method="post" action="AddEmployeeServlet" class="form1">
		<table>
			<tr>
				<td>Employee Name:</td><td><input type="text" required name="empName"></td>
			</tr>
			<tr>
				<td>Employee Address:</td><td><input type="text" required name="empAddress"></td>
			</tr>
			<tr>
				<td>Years of Experience:</td><td><input type="number" required name="yearsOfExperience"></td>
			</tr>
			<tr>
				<td>Hours Worked:</td><td><input type="number" step=0.01 required name="hWorked"></td>
			</tr>
			<tr>
				<td></td><td><input type="submit" value="Add"></td>
			</tr>
		</table>
	</form>
</div>

<button onclick="goBack()">Go Back</button>



</body>
</html>