<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

    
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View Employee</title>

<script>
function goBack() {
  window.history.back();
}
</script>
</head>
<body>
<h1>View Employee</h1>

<table>
<tr>
	<th>Name</th>
	<th>Address</th>
	<th>Years of Experience</th>
	<th>Wages</th>
</tr>
	<c:forEach items="${eList}" var="i">
			<tr>
				<td>${i.getEmployeeName()}</td>
				<td>${i.getEmployeeAddress()}</td>
				<td>${i.getYearOfExperience()}</td>
				<td>${i.getWages()}</td>
			</tr>
 </c:forEach>

</table>




<button onclick="goBack()">Go Back</button>


</body>
</html>