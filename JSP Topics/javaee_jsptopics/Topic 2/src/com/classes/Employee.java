package com.classes;

public class Employee {

	
	private String employeeName;
	private String employeeAddress;
	private int yearOfExperience;
	private double wages;
	
	
	public Employee() {
		super();
	}
	
	public Employee(String employeeName, String employeeAddress, int yearOfExperience, double wages) {
		super();
		this.employeeName = employeeName;
		this.employeeAddress = employeeAddress;
		this.yearOfExperience = yearOfExperience;
		this.wages = wages;
	}
	
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getEmployeeAddress() {
		return employeeAddress;
	}
	public void setEmployeeAddress(String employeeAddress) {
		this.employeeAddress = employeeAddress;
	}
	public int getYearOfExperience() {
		return yearOfExperience;
	}
	public void setYearOfExperience(int yearOfExperience) {
		this.yearOfExperience = yearOfExperience;
	}
	public double getWages() {
		return wages;
	}
	public void setWages(double wages) {
		this.wages = wages;
	}
	
	
	
	
	
	
}
