package com.servlets;

import java.io.IOException;

import db.utils.DbUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/AddEmployeeServlet")
public class AddEmployeeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddEmployeeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException {
	
    	String empName = request.getParameter("empName");
    	String empAddress = request.getParameter("empAddress");
    	int yearOfExperience = Integer.parseInt(request.getParameter("yearsOfExperience"));
    	double hWorked = Double.parseDouble(request.getParameter("hWorked"));

    	DbUtils db = new DbUtils();
    	
    	double wages = calculateWages(yearOfExperience, hWorked);
    	
    	boolean flag = false;
    	String address = "";
    	
    	 flag = db.addEmployee(empName, empAddress, yearOfExperience, wages);
    	
    	 
    	 if(flag) 
    		 address = "index.jsp";
    	 else
    		 address = "AddEmployee.jsp";

    	 
         RequestDispatcher dispatcher = request.getRequestDispatcher(address);
         dispatcher.forward(request, response);
    	 
    	 
    }

    
    double calculateWages(int yearsExperience, double hWorked) {
    	
    	double wages = 0.0;
    	//		        0 - 1 - 2					
    	int[] payRate = {20,50,90};


    	if(yearsExperience < 2) {
    			if(hWorked > 40)     				
    				wages = (40 + (hWorked -40))*1.5*payRate[0];
    			else 
    				wages = hWorked*payRate[0];
    	}else if((yearsExperience > 2) && (yearsExperience <= 5)) {
	    		if(hWorked > 40)     				
					wages = (40 + (hWorked -40))*1.5*payRate[1];
	    		else 
					wages = hWorked*payRate[1];
    	}else {
    		
	    		if(hWorked > 40)     				
					wages = (40 + (hWorked -40))*1.5*payRate[2];
	    		else 
	    			wages = hWorked*payRate[2];
    	}
    	
    	return wages;
    }
    
    
    
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	try {
			processRequest(request,response);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			processRequest(request, response);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
