package db.utils;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetProvider;
import javax.swing.JOptionPane;

import com.classes.Employee;

public class DbUtils {

	 //Class.forName("com.mysql.cj.jdbc.Driver");
	
	
    public JdbcRowSet _rs = null;
    private String USERNAME = null;
    private String PASS = null;
    private String SQL = null;
    private String URL = null;
    private Connection conn = null;
    private PreparedStatement ps = null;
    
    
	public boolean addEmployee(String eName, String eAddress, int yearsExperience, double wages) throws ClassNotFoundException{
			
		boolean flag = false;
		
		try { 
			
		Class.forName("com.mysql.jdbc.Driver");	
		String SQL = "INSERT INTO employee (name, address, yearOfExperience, wages) VALUES(?,?,?,?)";
          USERNAME = "root";
          PASS = "";
          URL = "jdbc:mysql://localhost:3306/jsp_topic2";
      
          Connection connection = DriverManager.getConnection(URL, USERNAME, PASS);

          ps = connection.prepareStatement(SQL);
          
          ps.setString(1, eName);
          ps.setString(2, eAddress);
          ps.setInt(3, yearsExperience);
          ps.setDouble(4,wages);
          
          if (ps.executeUpdate() > 0) {
              flag =  true;
          } else {
              flag = false;
          }
		}catch(SQLException ex) {
			System.out.print( "[ERROR - addEmployee]\n"+ex.getMessage());
		}
		
		return flag;
	} // end addEmployee

	public List<Employee> getAllEmployee() throws ClassNotFoundException  {
		
		List <Employee> eList = new ArrayList<Employee>();
		Employee e = null;
		try {
		
		  Class.forName("com.mysql.jdbc.Driver");
			_rs = RowSetProvider.newFactory().createJdbcRowSet();
			
		 String SQL = "SELECT name, address, yearOfExperience, wages FROM employee";
			
          USERNAME = "root";
          PASS = "";
          URL = "jdbc:mysql://localhost:3306/jsp_topic2";
    
          _rs = RowSetProvider.newFactory().createJdbcRowSet();
          _rs.setUrl(URL);
          _rs.setUsername(USERNAME);
          _rs.setPassword(PASS);
          _rs.setCommand(SQL);
          _rs.execute();
          
			while(_rs.next()) {
				e = new Employee();
				e.setEmployeeName(_rs.getString("name"));
				e.setEmployeeAddress(_rs.getString("address"));
				e.setWages(_rs.getDouble("wages"));
				e.setYearOfExperience(_rs.getInt("yearOfExperience"));
				
				eList.add(e);
			}

		}catch(SQLException ex) {
			System.out.println("\n[ERROR get all employees]: "+ex.getMessage());
		}
		return eList;
	} // end get all employees


	
}
