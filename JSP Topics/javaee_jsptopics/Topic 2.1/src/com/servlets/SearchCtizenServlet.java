package com.servlets;


import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.DB.DbUtil;
import com.classes.Citizen;

/**
 * Servlet implementation class SearchCtizenServlet
 */
@WebServlet("/SearchCtizenServlet")
public class SearchCtizenServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchCtizenServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
 
    protected void ProcessRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {


    	int id = Integer.parseInt(request.getParameter("id"));
    	
    	DbUtil db = new DbUtil();
    	String page = "";
    	String message = "";
    	Citizen c = new Citizen();
    	

    	c = db.getTaxPayer(id);
		
    	if(c.getId() == 0)
    	{
    		message = "Wrong ID";
    		request.setAttribute("messageErr", message);
    		page = "index.jsp";
    	}
    	else
    	{
    		page = "ViewTaxPayer.jsp";
    		request.setAttribute("c", c);
        	
    	}
 
        RequestDispatcher dispatcher = request.getRequestDispatcher(page);
        dispatcher.forward(request, response);
    }

    
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			ProcessRequest(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			ProcessRequest(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	}

}
