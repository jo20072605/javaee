package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.DB.DbUtil;


/**
 * Servlet implementation class AddCitizenServlet
 */
@WebServlet("/AddCitizenServlet")
public class AddCitizenServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddCitizenServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException {
	
		
		String name = request.getParameter("empName");
		String address = request.getParameter("empAddress");
		double totalIncome = Double.parseDouble(request.getParameter("totalIncome"));
		double taxToPay = calculateIncomeTax(totalIncome);
		
		String message = "";
		String page = "";

		PrintWriter out = response.getWriter();
		
		DbUtil db = new DbUtil();
		
		 if(db.addTaxPayerToDB(name,address,totalIncome,taxToPay))
		 {
	
			 message = "Data saved successfully....!!";
			 
			 page = "index.jsp";
		 }
			else
				message = "Sorry..!! Got an exception.";
		
		
		 
		request.setAttribute("message", message);
        RequestDispatcher dispatcher = request.getRequestDispatcher(page);
        dispatcher.forward(request, response);
		
		
	}

    
	double calculateIncomeTax(double tIncome) {
		
		double taxToPay = 0.0;

		if(tIncome <= 180000.00) {
			
			 taxToPay = tIncome;
		}else if ((tIncome >= 180000.00)&&(tIncome <=500000.00))
		{
			double lVar = tIncome - 180000.00;
			taxToPay = lVar * 0.10;
			
		}if ((tIncome > 500000.00)&&(tIncome <=800000.00))
		{
			double lVar = tIncome - 500000.00;
			taxToPay = 32000.00 + (lVar * 0.20);
		}
		if (tIncome > 800000.00)
		{
			double lVar = tIncome - 800000.00;
			taxToPay = 92000.00 + (lVar * 0.30);
		}		
		return taxToPay;
	}
	
	
    
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			processRequest(request, response);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
