package com.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.DB.DbUtil;
import com.classes.Citizen;

/**
 * Servlet implementation class ShowAllTaxPayers
 */
@WebServlet("/ShowAllTaxPayers")
public class ShowAllTaxPayers extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShowAllTaxPayers() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void processResquest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {

		
		String page = "";
		
		
		List<Citizen> cList = new ArrayList<Citizen>();
		DbUtil db = new DbUtil();
		
		
		cList = db.getAllTaxPayers();
		
//		if(cList.isEmpty()) 
//			request.setAttribute("data", "No taxpayers to show");
//		else 		
			request.setAttribute("eList", cList);
		
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("ViewTaxPayers.jsp");
        dispatcher.forward(request, response);
	}

    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			processResquest(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			processResquest(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
