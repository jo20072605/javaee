package com.classes;

public class Citizen {

	private int id;
	private String name;
	private String address;
	private double total_imcome;
	private double taxToPay;

	public Citizen() {
		super();
	}

	public Citizen(int id, String name, String address, double total_imcome, double taxToPay) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.total_imcome = total_imcome;
		this.taxToPay = taxToPay;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public double getTotal_imcome() {
		return total_imcome;
	}
	
	public void setTotal_imcome(double total_imcome) {
		this.total_imcome = total_imcome;
	}
	
	public double getTaxToPay() {
		return taxToPay;
	}
	
	public void setTaxToPay(double taxToPay) {
		this.taxToPay = taxToPay;
	}




}
