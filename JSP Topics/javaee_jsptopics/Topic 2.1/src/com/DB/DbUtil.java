package com.DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetProvider;

import com.classes.Citizen;

public class DbUtil {

	private final String URL = "jdbc:mysql://localhost:3306/jsp_topic2";
	private final String USERNAME = "root";
	private final String PASS = "";
	private String SQL;
	private PreparedStatement ps = null;
	public JdbcRowSet _RS = null;

	
	
	public List<Citizen> getAllTaxPayers() throws SQLException {

		List<Citizen> cList = new ArrayList<Citizen>();
		Citizen c = null;

		try {

			Class.forName("com.mysql.jdbc.Driver");
			SQL = "SELECT * FROM employeeTax";
			_RS = RowSetProvider.newFactory().createJdbcRowSet();
			_RS.setUrl(URL);
			_RS.setCommand(SQL);
			_RS.setUsername(USERNAME);
			_RS.setPassword(PASS);
			_RS.execute();
			
			while (_RS.next()) {

				c = new Citizen(_RS.getInt("id"), 
								_RS.getString("name"),
								_RS.getString("address"),
								_RS.getDouble("total_imcome"),
								_RS.getDouble("taxToPay"));
				cList.add(c);
			}

		} catch (Exception ex) {

			System.out.print("DBUtil ERROR: " + ex.getMessage()+"\n");
		}
		return cList;
	}// end get all tax payers


	
	public Citizen getTaxPayer(int id) throws SQLException {
		Citizen c =  new Citizen();;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			SQL = "SELECT * FROM employeeTax WHERE id = ?";
			_RS = RowSetProvider.newFactory().createJdbcRowSet();
			_RS.setUrl(URL);
			_RS.setUsername(USERNAME);
			_RS.setPassword(PASS);
			_RS.setCommand(SQL);
			_RS.setInt(1, id);
			_RS.execute();

			while (_RS.next()) {
			
				c.setId(_RS.getInt(1));
				c.setName(_RS.getString(2));
				c.setAddress(_RS.getString(3));
				c.setTotal_imcome(_RS.getDouble(4));
				c.setTaxToPay(_RS.getDouble(5));
			}

		} catch (Exception ex) {

			System.out.print("DBUtil ERROR: " + ex.getMessage()+"\n");
		}
		return c;
	}// end getTaxPayer

	public boolean addTaxPayerToDB(String eName, String eAddress, double totalIncome, double taxToPay)
			throws ClassNotFoundException {

		boolean flag = false;

		try {

			Class.forName("com.mysql.jdbc.Driver");
			String SQL = "INSERT INTO employeeTax (name, address, total_imcome, taxToPay) VALUES(?,?,?,?)";
		
			Connection connection = DriverManager.getConnection(URL, USERNAME, PASS);
			ps = connection.prepareStatement(SQL);

			ps.setString(1, eName);
			ps.setString(2, eAddress);
			ps.setDouble(3, totalIncome);
			ps.setDouble(4, taxToPay);
		

			if (ps.executeUpdate() > 0) {
				flag = true;
			} else {
				flag = false;
			}
		} catch (SQLException ex) {
			System.out.print("[ERROR - add citizen]\n" + ex.getMessage()+"\n");
		}

		return flag;
	} // end addTaxPayerToDB()
}
