<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tax Calculator</title>


<script type="text/javascript" src="JS/jquery-1.4.2.min.js"></script>
<script type="text/javascript">
   

$(document).ready(function() {
     $('#submitID').click(function(e) {
     e.preventDefault();
     var ajaxdata = $("#country").val();
     var value ='county='+ajaxdata;

     $.ajax({
     url: "AddCitizenServlet",
     //type: "post",
     data: value,
     cache: false,
     success: function(data) {
     $("#country").val('');
     $("#message").html(data).slideDown('slow');
     }
     });
});
});

$(document).ready(function() {
    $('#submitID_search').click(function(e) {
    e.preventDefault();
    var ajaxdata = $("#country").val();
    var value ='county='+ajaxdata;

    $.ajax({
    url: "AddCitizenServlet",
    //type: "post",
    data: value,
    cache: false,
    success: function(data) {
    $("#country").val('');
    $("#message").html(data).slideDown('slow');
    }
    });
});
});


</script>


</head>
<body>

<h1>Calculate tax</h1>
<hr>

<div id="formDiv">
	<form method="post" action="AddCitizenServlet" class="form1">
		<table>
			<tr>
				<td>Employee Name:</td><td><input type="text" required name="empName"></td>
			</tr>
			<tr>
				<td>Employee Address:</td><td><input type="text" required name="empAddress"></td>
			</tr>
			<tr>
				<td>Total Income:</td><td><input type="number" required name="totalIncome"></td>
			</tr>
			<tr>
				<td></td><td><input id="submitID" type="submit" value="Add"></td>
			</tr>
		</table>
	</form>
	<br><font face="verdana" size="2"><div id="message">${message}</div></font>
	
</div>

<!-- ----------------- SEARCH FOR CITIZEN ------------------- -->
<div>
	<form method="post" action="SearchCtizenServlet" class="form1">
		<table>
			<tr>
				<td>Citizen ID:</td><td><input type="number" required name="id"></td>
			</tr>
			<tr>
				<td></td><td><input id="submitID_Serach" type="submit" value="Search"></td>
			</tr>
		
		</table>
		
	</form>
	<br><font face="verdana" size="2"><div id="messageErr">${messageErr}</div></font>
</div>




<!-- ----------------- show all  CITIZEN ------------------- -->
<div>
	<form method="post" action="ShowAllTaxPayers" class="form1">
		<table>
			<tr>
				<td></td><td><input id="submitID_ShowAll" type="submit" value="Show All Tax Payers"></td>
			</tr>
		</table>
	</form>

</div>

</body


>
</html>