<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tax Calculator</title>


<script type="text/javascript" src="JS/jquery-1.4.2.min.js"></script>

<script>
function goBack() {
  window.history.back();
}
</script>



</head>
<body>
<button onclick="goBack()">Go Back</button>
<h1>View All Tax Payers</h1>
<hr>

<table>
	<tr>
		<th>ID</th>
		<th>Name</th>
		<th>Address</th>
		<th>Total income</th>
		<th>Tax to Pay</th>
	</tr>
	
		 <c:forEach items="${eList}" var = "i" >
		 <tr>
		 		<td>${i.getId()}</td>
				<td>${i.getName()}</td>
				<td>${i.getAddress()}</td>
				<td>${i.getTotal_imcome()}</td>
				<td>${i.getTaxToPay()}</td>				         
	 	</tr>
	 	</c:forEach>
	
</table>





</body


>
</html>