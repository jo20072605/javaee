<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View tax payer details</title>


<script type="text/javascript" src="JS/jquery-1.4.2.min.js"></script>


<script>
function goBack() {
  window.history.back();
}
</script>
</head>
<body>

<h1>View Tax Payer</h1>
<hr>
<button onclick="goBack()">Go Back</button>
<table>
	<tr>
		<th>ID</th>
		<th>Name</th>
		<th>Address</th>
		<th>Total income</th>
		<th>Tax to Pay</th>
	</tr>
	<tr>
		<td>${c.getId()}</td>
		<td>${c.getName()}</td>
		<td>${c.getAddress()}</td>
		<td>${c.getTotal_imcome()}</td>
		<td>${c.getTaxToPay()}</td>				         
	</tr>
</table>


</body>
</html>