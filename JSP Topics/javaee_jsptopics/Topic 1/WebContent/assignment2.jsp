<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Topic 1 - Assignment 2</title>
</head>
<body>


	<table border="1">
		<thead>
			<tr>
				<td>Student name</td>
				<td>Avg Marks</td>
			</tr>
		</thead>
		<tr>
			<td>Joao</td>
			<td>99</td>
		</tr>
		<tr>
			<td>Maria</td>
			<td>55</td>
		</tr>
		<tr>
			<td>Carlos</td>
			<td>78</td>
		</tr>
		<tr>
			<td>Ram</td>
			<td>80</td>
		</tr>
		<tr>
			<td>Elisa</td>
			<td>35</td>
		</tr>

	</table>



	<h2>Result:</h2>

	<%
		class Student {

			String name;
			int mark;

			public Student(String name, int mark) {
				this.mark = mark;
				this.name = name;
			}
		}

		List<Student> sList = new ArrayList<Student>();

		sList.add(new Student("Joao", 99));
		sList.add(new Student("Maria", 55));
		sList.add(new Student("Carlos", 78));
		sList.add(new Student("Ram", 80));
		sList.add(new Student("Elisa", 35));

		for (int i = 0; i < sList.size(); i++) {

			if (sList.get(i).mark >= 80)
				out.println(sList.get(i).name + " has Grade A<br>");
			
			if ((sList.get(i).mark >= 70) && (sList.get(i).mark < 80))
				out.println(sList.get(i).name + " has Grade B<br>");

			if ((sList.get(i).mark >= 60) && (sList.get(i).mark < 70))
				out.println(sList.get(i).name + " has Grade C<br>");

			if ((sList.get(i).mark >= 50) && (sList.get(i).mark < 60))
				out.println(sList.get(i).name + " has Grade D<br>");
			
			if (sList.get(i).mark < 50)
				out.println(sList.get(i).name + " has Grade F<br>");
		}
	%>

</body>
</html>