package classes;

import java.util.ArrayList;
import java.util.List;

public class Salary {

	
	public Salary() {
	
	}
	
	private double sal;
	private int empID;

	
	public Salary(double sal, int empID) {
		super();
		this.sal = sal;
		this.empID = empID;
	}
	public double getSal() {
		return sal;
	}
	public void setSal(double sal) {
		this.sal = sal;
	}
	public int getEmpID() {
		return empID;
	}
	public void setEmpID(int empID) {
		this.empID = empID;
	}
	
	
}
