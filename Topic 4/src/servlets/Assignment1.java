package servlets;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Assignment1
 */
@WebServlet("/Assignment1")
public class Assignment1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Assignment1() {
        super();
        // TODO Auto-generated constructor stub
    }

    // process the request
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//setting up the cookies
		Cookie cUsername = null;
		Cookie cPass= null;
		
		String error = "";
		String page = "";
		String username = request.getParameter("username");
		String pass = request.getParameter("pass");
		
	
		// check for post back
		if ("POST".equalsIgnoreCase(request.getMethod())) {	
	
			cUsername = new Cookie("username", username);
			cPass = new Cookie("pass", pass);
	
			cUsername.setMaxAge(60*60*24*365);
			response.addCookie(cUsername);
			response.addCookie(cPass);
		
			if(cUsername.getValue().equals("0")&&cPass.getValue().equals("0"))
				error = "passed";
			else{
					error = "Wrong detail";
					page = "login.jsp";
				}
			}
			else 
				page ="login.jsp";
			
		
		request.setAttribute("error", error);    	
		RequestDispatcher rd=request.getRequestDispatcher(page);
		rd.forward(request, response);
	}

    
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}
