package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Assignment3_1
 */
@WebServlet("/Assignment3_1")
public class Assignment3_1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Assignment3_1() {
        super();
        // TODO Auto-generated constructor stub
    }



    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    
    	response.setContentType("test/html");
    	PrintWriter out = response.getWriter();
    	
    	String name = request.getParameter("userName");
    	
    	out.print("Hello"+name);
    	out.close();
  
    }
    
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}
