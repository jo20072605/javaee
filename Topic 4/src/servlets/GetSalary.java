package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.Salary;
import classes.User;

/**
 * Servlet implementation class De
 */
@WebServlet("/GetSalary")
public class GetSalary extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetSalary() {
        super();
        // TODO Auto-generated constructor stub
    }



    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    
    	
    	HttpSession s = request.getSession();
    	
    	User user = (User) s.getAttribute("user");
    	List<Salary> myList = (List<Salary>) s.getAttribute("sList");
    	
    	
	
    s.setAttribute("myList", myList);	
    
	RequestDispatcher rd=request.getRequestDispatcher("home-page.jsp");
	rd.forward(request, response);
    	
    }
    
    
    
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}
