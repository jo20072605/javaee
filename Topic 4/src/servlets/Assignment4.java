package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.Salary;
import classes.User;

/**
 * Servlet implementation class Assignment4
 */
@WebServlet("/Assignment4")
public class Assignment4 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	
	List<User> uList;
	List<Salary> sList;
	
    public Assignment4() {
        super();
     
        User u = new User();
        Salary s = new Salary();
        
        
    uList = new ArrayList<User>();
    sList = new ArrayList<Salary>();
        uList.add(new User(0,"0","0"));
        uList.add(new User(1,"1","1"));
        uList.add(new User(2,"2","2"));
        uList.add(new User(3,"3","3"));
        
        sList.add(new Salary(2000.2, 0));
        sList.add(new Salary(2000.2, 1));
        sList.add(new Salary(2000.2, 2));
        sList.add(new Salary(2000.2, 3));
        
        
        // TODO Auto-generated constructor stub
    }


    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    
    	String username = request.getParameter("username");
    	String pass = request.getParameter("pass");
    	
    	String page = "";
    	String message = "";
    	

    	
    	
    	for(User us: uList) {
    		
    		if(us.getUsername().equals(username) && us.getPassword().equals(pass))
    		{
    		 HttpSession session = request.getSession();
    		
    		 
    		User user = new User();
    		
    		 user = uList.stream().filter(x -> (x.getUsername() == null ? username == null : x.getUsername().equals(username))).findFirst().get();
    		 session.setAttribute("user", user);
    		 session.setAttribute("sList", sList);
    		
    		page = "home-page.jsp";
    		
    		}else
    			message = "Wrong details";	
    	}
    
		request.setAttribute("message", message);    	
		RequestDispatcher rd=request.getRequestDispatcher(page);
		rd.forward(request, response);
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}
