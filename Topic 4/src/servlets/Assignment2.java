package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Assignment2
 */
@WebServlet("/Assignment2")
public class Assignment2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
    public Assignment2() {
        super();
        // TODO Auto-generated constructor stub
    }


    
    
 

    // process the request
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String page = "";
		String username = "";
		String pass = "";
		Cookie cUsername = null;
	 
		String message = "";
		
		Cookie[] cookies = request.getCookies();
		
		// handle is its post back
		if ("POST".equalsIgnoreCase(request.getMethod()))
			 {
			
			username = request.getParameter("username");
			pass = request.getParameter("pass");
			
			if(username.equals("0")&&pass.equals("0")) {
				for(Cookie c: cookies) {
					
					if(c.getName().equals(username))
					{
						message = "Welcome back, "+username+"!";
						
						break;
					}
					else
					{
						cUsername = new Cookie(username, username);
						cUsername.setMaxAge(60*60*24);
						response.addCookie(cUsername);	
						message = "Welcome "+username;
						 
					}
				}
				request.setAttribute("flag", true);   
				page = "home.jsp";
				 
			}
			else {
				message = "Wrong details!";
				page = "login.jsp";
			}
			
		

		}	else
				page = "login.jsp";

		
		request.setAttribute("message", message);    	
		RequestDispatcher rd=request.getRequestDispatcher(page);
		rd.forward(request, response);
	}
    
	
	
	
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}
