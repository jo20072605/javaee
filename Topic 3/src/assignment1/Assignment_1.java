package assignment1;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Assignment_1")
public class Assignment_1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Assignment_1() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    	int num = Integer.parseInt(request.getParameter("num"));
    	String page = "";
    	
    	
    	
    	if(request== null) 
    			page = "index.jsp";
    	else {
		    	if( num < 10)
		    		page = "page1.jsp";
		    	else if((num > 10) && (num < 99))
		    		page = "page2.jsp";
		    	else
		    		page = "page3.jsp";
		    	
    	}
		request.setAttribute("num", num);    	
		RequestDispatcher rd=request.getRequestDispatcher(page);
		rd.forward(request, response);
		
	}
    
    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
